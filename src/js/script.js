import jQuery from 'jquery';
import slick from 'slick-carousel';

'use strict';

const message = {
    loading: 'Загрузка...',
    success: 'Спасибо! Скоро мы с вами свяжемся',
    failure: 'Что-то пошло не так...'
};

const postData = async(url, data) => {

    document.querySelector('.filter-status').textContent = message.loading;

    let result = await fetch(url, {
        method: "POST",
        body: data
    });

    return await result.json();
};

let arForms = document.querySelectorAll('form');

if (arForms.length > 0) {

    arForms.forEach(element => {
        element.addEventListener('submit', (e) => {
            e.preventDefault();

            console.log(e.target.dataset.name);
        });
    });
}

let arToogleNext = document.querySelectorAll('.toogle-next');

if (arToogleNext.length > 0) {

    arToogleNext.forEach(element => {
        element.addEventListener('click', (e) => {
            e.preventDefault();

            let nextElement = element.nextElementSibling;
            nextElement.classList.toggle("d-none");
            nextElement.classList.toggle("d-xs-none");
            nextElement.classList.toggle("d-sm-none");
        });

    });
}

function animateValue(obj, start, end, duration) {
    if (start === end)
        return;

    var range = end - start;

    var current = start;

    var increment = end > start ? 1 : -1;

    var stepTime = Math.abs(Math.floor(duration / range));

    var timer = setInterval(function() {

        current += increment;

        obj.innerHTML = current;

        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}

function numAnimate(item) {

    jQuery(function($) {

        $(item).hide();

        $(item)
            .show(1000, function() {
                let num = item.dataset.num;
                let text = item.dataset.text;

                item.innerHTML = '';

                let numBlock = document.createElement('div');
                let textBlock = document.createElement('div');

                numBlock.classList = 'num-block';
                textBlock.classList = 'text-block';

                let duration = 100;

                if (num < 1000)
                    duration = 10000;

                if (num > 100000)
                    duration = 10;

                numBlock.innerHTML = (animateValue(numBlock, 0, num, duration) == 'undefind') ? animateValue(numBlock, 1, num, duration) : 0;
                textBlock.innerHTML = text;

                item.append(numBlock);
                item.append(textBlock);
            });
    });
}

function clickHiddenBlock(item) {

    let LinkMore = document.getElementById('link-more');
    let LinkLess = document.getElementById('link-less');

    LinkMore.addEventListener('click', (e) => {
        e.preventDefault();

        item.classList.toggle("visually-hidden");
        LinkMore.classList.toggle("visually-hidden");

    });

    LinkLess.addEventListener('click', (e) => {
        e.preventDefault();

        item.classList.toggle("visually-hidden");
        LinkMore.classList.toggle("visually-hidden");
    });
}

var achivmentItems = document.querySelectorAll('.about-us-achivment-item');
var certificationCenterItems = document.querySelectorAll('.certification-center-item');

achivmentItems.forEach(item => {
    numAnimate(item);
});

certificationCenterItems.forEach(item => {
    numAnimate(item);
});

var aboutUsMessagesBlocks = document.querySelectorAll('.about-us-target-text-block');
var sharesMessagesBlocks = document.querySelectorAll('.shares-hide-text-block');
var certificationMessagesBlocks = document.querySelectorAll('.certification-hide-text-block');

if (aboutUsMessagesBlocks.length > 0) {
    aboutUsMessagesBlocks.forEach(item => {

        clickHiddenBlock(item);
    });
}
if (sharesMessagesBlocks.length > 0) {
    sharesMessagesBlocks.forEach(item => {

        clickHiddenBlock(item);
    });
}
if (certificationMessagesBlocks.length > 0) {
    certificationMessagesBlocks.forEach(item => {

        clickHiddenBlock(item);
    });
}

var showHideFilter = document.getElementById('showHideFilter');

if (showHideFilter != null) {

    showHideFilter.addEventListener('click', (e) => {
        e.preventDefault();

        let productFilterBlocks = document.querySelectorAll('.product-filter');

        productFilterBlocks.forEach(filterBlock => {
            filterBlock.classList.toggle("visually-hidden");
        });
    });
}


var projectsFilterBlocks = document.querySelectorAll('.projects-filter-block');

if (projectsFilterBlocks.length > 0) {

    projectsFilterBlocks.forEach(projectFilterBlock => {

        let filterLinks = projectFilterBlock.querySelectorAll('.menu-link');

        filterLinks.forEach(link => {

            link.addEventListener('click', (e) => {

                e.preventDefault();

                let elementsBlocks = document.querySelectorAll('.company-element-in');

                let elementBlock = elementsBlocks[0];

                let statusMessage = document.createElement('div');

                statusMessage.classList.add('filter-status');

                elementBlock.prepend(statusMessage);

                let objectPost = {};

                objectPost.branch = link.dataset.branch;

                let strSend = JSON.stringify(objectPost);

                postData('server.php', strSend)
                    .then(result => {
                        console.log(result);
                        statusMessage.textContent = message.success;
                    })
                    .catch(() => {
                        statusMessage.textContent = message.failure;
                        //   window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", urlPath);
                    })
                    .finally(() => {
                        setTimeout(() => {
                            statusMessage.remove();
                        }, 5000);
                    });
            });


        });
    });
}


jQuery(function($) {
    $(document).ready(function() {
        $('.about-us-target-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            arrows: true,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('.graduates-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
});